synfig (1.2.2+dfsg-3) unstable; urgency=medium

  * Team upload
  * debian/patches: Apply upstream patch to fix build with icu 67 (Closes:
    #962069)
  * Bump Standards-Version

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 08 Jun 2020 23:41:58 +0200

synfig (1.2.2+dfsg-2) unstable; urgency=medium

  * Re-build to fix segfaults due to ABI breakage somewhere (Closes: #941633).
  * Build using '--with-opegl';
    Build-Depends += "libgl1-mesa-dev"
  * DH to version 12
  * "watch" file to track GitHub releases.
    SourceForge tarball is incomplete...

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 18 Dec 2019 14:52:12 +1100

synfig (1.2.2-1) unstable; urgency=medium

  [ Alexis Bienvenüe ]
  * New reproducible buiild patch (Closes: #820072).

  [ Dmitry Smirnov ]
  * New upstream release.
  * Standards-Version: 4.3.0.
  * debhelper & compat to version 11.
  * VCS URLs to Salsa.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 31 Jan 2019 17:41:06 +1100

synfig (1.2.1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version.
  * Rename libsynfig0v5 to libsynfig0a. Upstream symbol version changes
    without bumping the soname.
  * Update (build) dependency on etl-dev. Closes: #895503.
  * Stop building the -dbg package.
  * Build-depend on intltool and libfftw3-dev.

 -- Matthias Klose <doko@debian.org>  Thu, 26 Apr 2018 13:09:54 +0200

synfig (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depends:
    - etl-dev (>= 0.04.18)
    + etl-dev (>= 0.04.19)
  * Disabled "no-mod_ffmpeg.patch".
  * New "c++11.patch" to fix FTBFS.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 27 Nov 2015 20:57:05 +1100

synfig (1.0-2) unstable; urgency=medium

  * Team upload.
  * Rename libsynfig0 to libsynfig0v5 for the GCC 5 transition. (Closes:
    #797994)

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 07 Sep 2015 20:03:32 +0200

synfig (1.0-1) unstable; urgency=low

  * New upstream release [April 2015].
  * Build-Depends:
    - etl-dev (>= 0.04.17)
    + etl-dev (>= 0.04.18)
    + libmlt-dev
    + libmlt++-dev
  * Dropped obsolete "hurd-path_max.patch".

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 07 May 2015 09:33:13 +1000

synfig (0.64.2-2) unstable; urgency=medium

  * New "hurd-path_max.patch" to fix FTBFS on GNU Hurd.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 18 Nov 2014 11:35:00 +1100

synfig (0.64.2-1) unstable; urgency=medium

  * New upstream release [October 2014].
  * Added "debian/gbp.conf".
  * Standards-Version: 3.9.6.
  * Breaks: synfigstudio (<< 0.64.2).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 20 Oct 2014 23:09:19 +1100

synfig (0.64.1-2) unstable; urgency=medium

  * Build using "--with-boost-libdir" to fix FTBFS with multi-arch Boost
    (Closes: #738379).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 10 Feb 2014 21:05:10 +1100

synfig (0.64.1-1) unstable; urgency=low

  * New upstream release [November 2013].
  * Standards to 3.9.5.
  * Build-Depends on new "etl-dev (>= 0.04.17)".
  * As per his request removed Denis Washington from Uploaders.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 21 Nov 2013 13:56:42 +1100

synfig (0.64.0-4) unstable; urgency=low

  * Switch to virtual packages in Build-Depends (Closes: #722938)
    + libjpeg8-dev --> libjpeg-dev
    + libtiff4-dev --> libtiff-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 15 Sep 2013 09:18:41 +1000

synfig (0.64.0-3) unstable; urgency=low

  * Dropped .symbols since they are not useful for C++ libraries.
  * Added new patch to skip "mod_ffmpeg"; thanks Sebastian Ramacher.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 14 Sep 2013 03:14:11 +1000

synfig (0.64.0-2) unstable; urgency=low

  * Commented unused libav Build-Depends (Closes: #721105).
  * Replaced unused "ffmpeg" with commented "libav-tools" in Depends
    of libsynfig0 (Closes: #721106).
  * Introduced .symbols and shlibs tightening (dh_makeshlibs -V).

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 13 Sep 2013 20:24:30 +1000

synfig (0.64.0-1) unstable; urgency=low

  * New upstream release [May 2013].
  * New Build-Depends:
    + libboost-all-dev
    + libcairo-dev
    + libpango1.0-dev
  * Standards to 3.9.4.
  * xz compression for source and binary packages.
  * Minor debian/watch update.
  * Updated my email address; bumped copyright years.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 14 May 2013 02:09:07 +1000

synfig (0.63.05-1) unstable; urgency=low

  * New maintainers (Closes: #665042)
  * New upstream release.
  * imported latest QA changes
  * Packaging update
    + to straight debhelper + dh-autoreconf
    + compat & debhelper to version 9
    + standards to 3.9.3
    + --as-needed to avoid needless linking
    + corrected Vcs-Browser URL
    - disable --with-libavcodec (fixes FTBFS)
    + build-deps recalculated
    + libsynfig-dev deps recalculated
    - removed *.la *.a from libsynfig-dev.install
    + set d-multimedia as Maintainer

 -- Dmitry Smirnov <onlyjob@member.fsf.org>  Sat, 12 May 2012 16:55:50 +1000

synfig (0.62.00-2) unstable; urgency=low

  * Bump dependencies on libsynfig0 to avoid self-induced uninstallability

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 24 Jan 2010 10:45:11 +0100

synfig (0.62.00-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Upgrade to a non-vulnerable version of ltdl and link against system
    library libltdl3; add build-dep on libltdl3-dev and force configure to
    use system library. Fix for CVE-2009-3736. (Closes: #559829)
  * Bump dep on etl-dev to >= 0.04.13 (needed since this version)
  * Bump standards-version to 3.8.3 (no changes needed)
  * Ship libsynfig.a, which was not installed before (not even by upstream)
  * Set section for package synfig-dbg to "debug" (thanks, lintian)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 24 Jan 2010 00:01:27 +0100

synfig (0.61.09-3) unstable; urgency=low

  * QA upload.
  * Change maintainer to Debian QA Group and remove all uploaders, see #538964
  * Add #include <cstdio> to src/synfig/time.cpp to fix FTBFS with gcc 4.4
    (Closes: #504949)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 06 Sep 2009 11:50:36 -0400

synfig (0.61.09-2) unstable; urgency=low

  * Upload to unstable after the lenny freeze
  * Will build against the new FFmpeg APIs (Closes: #487639)
    - build-dep on libswscale-dev
  * Improve the libsynfig0 short description (Closes: #493626)
  * Don't recompress .sifz files in synfig-examples (Closes: #506894)
  * Switch to debhelper compat level 7
  * Switch from dh_clean -k to dh_prep
  * Link to the correct versions of the GPL/LGPL
  * Remove execute permission from sifz files
  * Add ${misc:Depends} to the dependencies

 -- Paul Wise <pabs@debian.org>  Wed, 25 Feb 2009 03:30:54 +0900

synfig (0.61.09-1) experimental; urgency=low

  [ Paul Wise ]
  * New upstream release
    - drop merged fix-upstream-1877061-avformat-52-FTBFS.patch
      - drop quilt build-dep, no longer needed
    - bump the etl-dev build-depends
  * Drop upstream ChangeLog since it was empty for this release
  * Wrap the uploaders, depends, suggests and build-deps fields
  * Support parallel builds
  * Now compliant with policy 3.8.0
  * libsynfig-dev depends on etl-dev should be versioned

  [ Cyril Brulebois ]
  * Fix FTBFS with new “3.0 (quilt)” source package format by using a
    strip level of 1 instead of 0 for the following patch, and by removing
    the “-p0” option from the series file. Thanks to Raphaël Hertzog for
    the notice (Closes: #485259).
     - fix-upstream-1877061-avformat-52-FTBFS.patch
  * Update my mail address.

 -- Paul Wise <pabs@debian.org>  Sun, 16 Nov 2008 15:36:08 +0900

synfig (0.61.08-3) unstable; urgency=low

  * Fix FTBFS with libavformat-dev version 52
  * Make the build process safe for parallel building

 -- Paul Wise <pabs@debian.org>  Wed, 21 May 2008 17:48:30 +0800

synfig (0.61.08-2) unstable; urgency=low

  * Brown paper bag release
  * Make synfig-examples conflicts/replace libsynfig-doc
  * Make the shlibs more restrictive to prevent symbols problems

 -- Paul Wise <pabs@debian.org>  Thu, 13 Mar 2008 19:19:56 +0900

synfig (0.61.08-1) unstable; urgency=low

  [ Cyril Brulebois ]
  * New upstream release.
  * debian/libsynfig0.install:
     - Add “usr/share/locale”, since upstream now ships locales.
  * debian/control:
     - Move the homepage from “synfig.com” to “synfig.org”.
     - Bump the Build-Depends on etl-dev from 0.04.10~rc3 to 0.04.11.
     - Update the “Depends:” line of synfig-dbg, from “libsynfig0” to
       “libsynfig0 | synfig” (both with ${binary:Version}) since this
       package contains the debugging symbols for both packages.
     - Bump Standards-Version from 3.7.2 to 3.7.3 (no changes needed).
  * debian/rules:
     - Remove “DH_COMPAT=4” before the dh_clean call, no reason to have
       that.
     - Copy config.{guess,sub} from /usr/share/misc in the config.status
       target, remove them in the clean target, and no longer keep a copy
       of the original ones.

  [ Paul Wise ]
  * Drop the doxygen documentation package and move the examples to a new
    synfig-examples package.
  * Build-depend on libmagick++9-dev for the new output target that can
    create optimised GIFs
  * Build-depend on libmng-dev for the MNG output target
  * Improve the manual page a bit; remove duplicate options, add new ones
  * Avoid running the configure script twice when building

 -- Paul Wise <pabs@debian.org>  Tue, 11 Mar 2008 02:21:44 +0900

synfig (0.61.07-1) unstable; urgency=low

  * New upstream release.
  * synfig-dbg should be extra (like ftpmaster overrides)

 -- Paul Wise <pabs@debian.org>  Thu, 11 Oct 2007 11:48:33 +1000

synfig (0.61.07~rc3-1) unstable; urgency=low

  [ Cyril Brulebois ]
  * New upstream release.
  * debian/control:
     - Rename XS-Vcs-* to Vcs-*, update Vcs-Browser to point to the log of the
       trunk.
     - Add a Homepage field.
     - Bump the versioned Build-Depends on etl-dev to the latest release,
       from 0.04.09 to 0.04.10~rc3.
  * debian/patches:
     - Drop amd64-fix-import, merged upstream.
     - Drop fix-empty-pastecanvas.patch, merged upstream as well.
     - Empty series file, and patch system, kept around, until some other
       patches are needed.

  [ Paul Wise ]
  * Fix crashes with empty PasteCanvas layers (closes upstream 1356449)
  * Improve the package descriptions and suggest the gui (Closes: #442039)
  * Use optimization level 2 now that upstream fixed the issue with it

 -- Paul Wise <pabs@debian.org>  Wed, 10 Oct 2007 10:21:17 +1000

synfig (0.61.06-2) unstable; urgency=low

  * Prevent import errors in synfigstudio on amd64 (closes upstream 1692825)
  * Fix sections so they correspond to ftpmaster overrides
  * Add Cyril Brulebois (KiBi) to the co-maintainers list
  * Don't ignore errors with make distclean
  * Add XS-Vcs URLs to the source package

 -- Paul Wise <pabs@debian.org>  Sat, 04 Aug 2007 18:40:24 +1000

synfig (0.61.06-1) unstable; urgency=low

  * New upstream release
    - fixes FTBFS in GCC 4.3 (Closes: #417720)
    - requrires the new etl, bump build-dep
    - enable the fixed libavcodec plugin
  * Change my email address now that I'm a Debian Developer
  * Update copyright information
    - libav headers are no longer shipped in the tarball
    - New contributors
  * Use sourceforge in the watch file and download page
  * Install the new upstream README, AUTHORS, ChangeLog.old
  * Use the current upstream version in the shlibs for now

 -- Paul Wise <pabs@debian.org>  Thu, 21 Jun 2007 11:04:49 +0100

synfig (0.61.05-8) unstable; urgency=low

  * Switch back to using the default g++ (4.1) (Closes: #383714)
  * Use -O1, makes composition loading work (Workaround #375080)

 -- Paul Wise <pabs3@bonedaddy.net>  Thu, 31 Aug 2006 11:16:49 +0800

synfig (0.61.05-7) unstable; urgency=low

  [ Paul Wise ]
  * Build with g++-4.0 because g++-4.1 4.1.1 miscompiles synfig
  * Update mod_ffmpeg for modern ffmpeg. Thanks to Luka Pravica.
  * Implement fontconfig support (Closes: #368733)
  * Use ${binary:Version} instead of the depreciated ${Source-Version}

  [ Fabian Fagerholm ]
  * Move example files to libsynfig-doc.
  [ Miguel Gea Milvaques ]
  * Added as Uploader.

 -- Paul Wise <pabs3@bonedaddy.net>  Sun, 16 Jul 2006 13:54:16 +0800

synfig (0.61.05-6) unstable; urgency=low

  [ Paul Wise ]
  * Explicitly disable mod_libavcodec (Closes: #374403)

 -- Fabian Fagerholm <fabbe@debian.org>  Thu, 22 Jun 2006 12:54:39 +0300

synfig (0.61.05-5) unstable; urgency=low

  [ Paul Wise ]
  * Disable -ffast-math and prevent crash on alpha (Closes: #367048)

  [ Fabian Fagerholm ]
  * Rerun ./bootstrap, this time using newer versions of autoconf and libtool.
  * debian/rules: provide a dummy build target to be policy compliant
    (policy 4.8).
  * debian/control: libsynfig-doc should be in section doc.
  * debian/control: bump standards-version.

 -- Fabian Fagerholm <fabbe@debian.org>  Sat, 17 Jun 2006 00:17:16 +0300

synfig (0.61.05-4) unstable; urgency=low

  [ Fabian Fagerholm ]
  * Change priority to extra because libdv-bin is extra. (Closes: #358423)

  [ Paul Wise ]
  * Change maintainer to the mailing list, move me to uploaders
  * Move .so symlinks for the plugins to libsynfig0 (Closes: #358242)
  * Add fix for alpha/amd64 FTBFS, b-d on fixed etl-dev (Closes: #359690)

 -- Fabian Fagerholm <fabbe@debian.org>  Sun,  2 Apr 2006 10:07:05 +0300

synfig (0.61.05-3) unstable; urgency=low

  [Fabian Fagerholm]
  * Drop alternative build-dependency on libetl-dev. (Closes: #357564)

 -- Fabian Fagerholm <fabbe@debian.org>  Sun, 19 Mar 2006 21:41:41 +0200

synfig (0.61.05-2) unstable; urgency=low

  [Paul Wise]
  * Rename -dev and -doc package (drop 0 from name).

 -- Fabian Fagerholm <fabbe@debian.org>  Wed, 15 Mar 2006 11:58:55 +0200

synfig (0.61.05-1) unstable; urgency=low

  [Paul Wise]
  * New upstream release
  * Depend on libetl-dev | etl-dev, etl-dev will be renamed soon

  [Fabian Fagerholm]
  * debian/control: libsynfig0-dev now depends on libetl-dev | etl-dev
  * Rerun ./bootstrap (reautotoolize/libtoolize).

 -- Fabian Fagerholm <fabbe@debian.org>  Sat,  4 Mar 2006 12:09:55 +0200

synfig (0.61.04-1) unstable; urgency=low

  * Initial release (Closes: #337516)

 -- Paul Wise <pabs3@bonedaddy.net>  Wed, 11 Jan 2006 11:17:11 +0800
